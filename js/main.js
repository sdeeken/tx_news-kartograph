var GlobeOpt = function() {
        this.lon0 = 20;
        this.lat0 = 35;
        this.lat1 = 30;
        this.lat2 = 50;
        this.dist = 2;
        this.up = 0;
        this.tilt = 0;
        this.proj = 'ortho';
        this.projstr = '+proj=lcc +lat_1=44.1 +lat_0=44.1 +lon_0=0 +k_0=0.999877499 +x_0=600000 +y_0=200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m',
        //this.flip = 0;
        this.offsetx = 0;
        this.offsety = 0;
        this.startx = 0;
        this.starty = 0;
        this.deltalon = 0;
        this.deltalat = 0;
        this.isdragged = false;
        this.firstclick = true;
    };
    var globeopt = new GlobeOpt();

$(function () { //ACHTUNG --> z.b. z24 (klappt das? 29.11.13 oli) hier schon geaendert
    


    //var
    var time = '';
    var timeStr = '';
    var timeMax = '';
    var newTime = '';

    $('html').css("position", "fixed");
    $('body').css("overflow", "hidden");
    $('#map').click();



    $('#loader_img').show();
    var windowWidth = $(window).width(); //retrieve current window width
    var windowHeight = $(window).height(); //retrieve current window height
    var mapwith = 4000;
    var maxX = ((windowWidth) - (mapwith)) * 0.8;
     console.info('maxX: '+maxX);
    //console.info('((windowWidth)/(mapwith/2)):'+((windowWidth)/(mapwith/2)));


    $('#map').draggable({
        containment: [maxX, -3193, -75, -580] //maxX maxY minX minY
    }); //map beweglich

    

    function mapposition() {
        //Mini Map Position definieren
        if (parseInt($('#map').css('left'), 10) > (-500)) {
            if (parseInt($('#map').css('top'), 10) > (-1030)) {
                $('#shadow').css({
                    backgroundPosition: '100% 100%'
                });
            } else if (parseInt($('#map').css('top'), 10) > (-2000)) {
                $('#shadow').css({
                    backgroundPosition: '100% 50%'
                });
            } else if (parseInt($('#map').css('top'), 10) < (-2000)) {
                $('#shadow').css({
                    backgroundPosition: '100% 0%'
                });
            }
        } else if (parseInt($('#map').css('left'), 10) > (-1650)) {
            if (parseInt($('#map').css('top'), 10) > (-1030)) {
                $('#shadow').css({
                    backgroundPosition: '50% 100%'
                });
            } else if (parseInt($('#map').css('top'), 10) > (-2000)) {
                $('#shadow').css({
                    backgroundPosition: '50% 50%'
                });
            } else if (parseInt($('#map').css('top'), 10) < (-2000)) {
                $('#shadow').css({
                    backgroundPosition: '50% 0%'
                });
            }
        } else if (parseInt($('#map').css('left'), 10) < (-1650)) {
            if (parseInt($('#map').css('top'), 10) > (-1030)) {
                $('#shadow').css({
                    backgroundPosition: '0% 100%'
                });
            } else if (parseInt($('#map').css('top'), 10) > (-2000)) {
                $('#shadow').css({
                    backgroundPosition: '0% 50%'
                });
            } else if (parseInt($('#map').css('top'), 10) < (-2000)) {
                $('#shadow').css({
                    backgroundPosition: '0% 0%'
                });
            }
        }
        //END Mini Map Position definieren
    };

    //start ReisePfad
    function pt(lon, lat) {
        return new Kartograph.LonLat(lon, lat);
    }

   function olghelp(info, moreinfo, divlink, h, w, targetposi1, targetposi2) {


        if ($(".ui-dialog") !== '') {
            $(".ui-dialog-content").dialog("close");
        }

        if (!info) {
            info = 'Hilfesystem';
        }
        if (!moreinfo) {
            divlink = $('.helpmain').attr('href'); 
            moreinfo = '<div style="width: 100%; display: inline-block; height: 520px; line-height: 520px; text-align: center;"><img src="http://spiel.fremdeweltganznah.de/fileadmin/htmltest/css/images/animated-overlay.gif"/></div>';
            h = 528;
            w = 492;
        }
        $("<div class='olghelp'>" + moreinfo + "</div>").dialog({
            resizable: false,
            title: false,
            height: h,
            width: w,
            close: function (event, ui) {
            },
            open: function () {

                //oli 6.2.14 // soundtest end
                $(this).parents('.ui-dialog').addClass('ui-big');

                if (divlink) {
					/*
                    // wenn ein link da ist, lade diesen ins dialog.popup
                    jQuery(".olghelp").load(divlink, function () {
                        $("a.btn").button(); //mach aus den a.btn "buttons"
                        }); //help load end
                        */
                        jQuery(".olghelp").text('hallo Welt' + divlink);
                         
                }

                //link = '#';
            }
        });
    } //olghelp end

    function handleKeys(e, ui) {
        var keycode = (e.keycode ? e.keycode : e.which),
            maxl = -65,
            maxt = -580,
            maxr = maxX,
            maxb = -3193,
            map = $('#map').position(); //maxX maxY minX minY

        switch (keycode) {
        case 37:
            if (map.left <= maxl) {
                $('#map').css({
                    'left': '+=10'
                });
            } else {
                olghelp('Das Ende der Welt');
            }
            break;
        case 38:
            if (map.top <= maxt) {
                $('#map').css({
                    'top': '+=10'
                });
            } else {
                olghelp('Das Ende der Welt');
            }
            break;
        case 39:
            if (map.left >= maxr) {
                $('#map').css({
                    'left': '-=10'
                });
            } else {
                olghelp('Das Ende der Welt');
            };
            break;
        case 40:
            if (map.top >= maxb) {
                $('#map').css({
                    'top': '-=10'
                });
            } else {
                olghelp('Das Ende der Welt');
            }
            break;
        case 72:
            olghelp();
            break;
        }
        cookiepx();
        mapposition();

    }
    $(document).on('keydown', handleKeys);
    //bewegen per maustasten & cookie bei dragstop setzen end

    var map = Kartograph.map('#map', 4000, 0);

    $.fn.qtip.defaults.style.classes = 'ui-tooltip-bootstrap';

    //map.proj = 'ortho';
    //map.projstr = '+proj=lcc +lat_1=44.1 +lat_0=44.1 +lon_0=0 +k_0=0.999877499 +x_0=600000 +y_0=200000 +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +pm=paris +units=m',
        
    map.loadMap(olgpath + 'svg/world.svg', function (data) { //ueberlappende namen fixed, oli 2.1.
        
        $('#loader_img').remove();
        map.addLayer('world', {
            styles: {
                stroke: '#eff'
            }
            //tooltips: function(data) { return data.name + ' ' +data['olglongnam']; }
        });
        map.getLayer('world').style('fill', function (data) { //die staaten haben als mapcolor13 def. farben von 1.0 bis 13.0 als ganzzahlen ;)
            //var color = (data['mapcolor13'])*1;
            var color = (data['mapcolor13']) * 1;

            var olgrgb = ('rgb(' + ((color + 10 * 2.6) + 30) + ',' + ((color + 10 * 1.3) + 30) + ',' + ((color * 1) + 0) + ')');

            var olgrgball = ('rgb(100,100,100)');
            return data.continent == 'Africa' ? olgrgb : olgrgball;
        });
   map.addFilter('oglow', 'glow', {
                    size: 1,
                    color: '#988',
                    strength: 1,
                    inner: false
                });
                map.getLayer('world').applyFilter('oglow');
        // Click Event für spätere Funktionen
        //map.getLayer('world').on('click', function(data, path, event) {  
        //reise(data.midlong, data.midlat);                 
        //userposition tmp
        //var userpositionArray = [data.midlong, data.midlat];
        //$.cookie('Userpos', JSON.stringify(userpositionArray));
        //var storedAry = JSON.parse($.cookie('Userpos'));
        //console.info($.cookie('Userpos'));
        //console.info(storedAry);
        // $.cookie('Userpos', (data.midlong, data.midlat), { path: '/', expires: 7 });
        //olghelp(data['olglongnam'],'hier: '+data['olglongnam']+'<br> lonLat (test) <br>'+data.midlong+' '+data.midlat,  280, 250);      
        //        olghelp(data['olglongnam'],'in english: '+data.name, 'http://de.wikipedia.org/wiki/'+data['olglongnam'], 280, 250);  

        //console.log(data);


        //console.info('name: '+data.name);
        //console.info('olgname: '+data.olgname);
        //console.info('olglongnam: '+data['olglongnam']);
        //});
        //END


/*
        map.addLayer('cities', {
            styles: {
                fill: '#3c42e0',
                'stroke-opacity': 0.1,
                'fill-opacity': 0.05
            }
        });
*/
        //console.log(map.getLayer('cities').getPathsData(function(d) { return [d.longitude, d.latitude]; }));
/*
        map.addSymbols({
            type: $K.Label,
            data: map.getLayer('cities').getPathsData(),
            location: function (d) {
                return [d.longitude, d.latitude];
            },
            style: 'font-family: Arial; fill:#cccccc; font-size: 12px; text-anchor:start;',
            offset: [0, 0],
            text: function (d) {
                return d.name;
            }
        });


        //test
        map.addSymbols({
            type: $K.Label,
            data: map.getLayer('world').getPathsData(),
            location: function (d) {
                return [d.midlong, d.midlat];
            },
            style: 'font-family: Arial; fill:#ffffff; font-size: 12px; text-anchor: middle;',
            offset: [0, 20],
            'stroke': 'black',
            text: function (d) {
                return (d.olglongnam);
            }
        });
        //test
*/
        map.addLayer('graticule', {
            styles: {
                stroke: 'rgba(0,0,0,0.2)'
            }
        });

        $('text:first').css({'display': 'none'});

        //start jsontest MapSymbols

        $.getJSON('data/news.json', function (data) {
            var points = [];
            $.each(data.items, function (i, item) {
                points.push([new $K.LonLat(item.longitude, item.latitude), item.title, item.media.m, item.description]);
                if (i == 50) return false; //wenn mehr als 50 icons auf der map sind, lass den rest weg
            });

            $.fn.qtip.defaults.style.classes = 'qtip-light';
            map.addSymbols({
                type: $K.Icon,
                data: points,
                location: function (d) {
                    return d[0];
                },
                class: 'mapicons',

                icon: function (d) {
                    return d[2];
                },
                iconsize: [90, 90],
                offset: [-90, -90],
                title: function (d) {
                    return d[4];
                },
                tooltip: function (d) {
                    return ['<h3>' + d[1] + '</h3>' + d[3]];
                }
            });
            /*
            $('.mapicons').mouseover(
                function () {
                    var quelle = $(this).attr("src");
                    var title = $(this).attr("title");
                    $(this).attr("src", title);
                    $(this).attr("title", quelle);
                }
            ).mouseout(
                function () {
                    var quelle = $(this).attr("src");
                    var title = $(this).attr("title");
                    $(this).attr("src", title);
                    $(this).attr("title", quelle);
                }
            );
            */
        })
            .done(function () { //map symbole geladen, jetzt im anschlus die jobs
                $.getJSON('data/news.json', function (data) { //jobs json
                    var points = [];
                    $.each(data.items, function (i, item) {
                        points.push([new $K.LonLat(item.longitude, item.latitude), item.title, item.media.m, item.country, item.link, item.longitude, item.latitude]);
                        //console.info('Test Geo Data #2: ' + data.items[0]);
                        //console.info('Test Geo Data: ' + points[i].item.longitude);
                        //if ( i == 100 ) return false;
                    });
                    //console.info('Test Geo Data #2: ' + data.items[0]);
                    map.addSymbols({
                        type: $K.Icon,
                        data: points,
                        location: function (d) {
                            return d[0];
                        },
                        icon: function (d) {
                            return d[2];
                        },
                        iconsize: [55, 55],
                        offset: [-35, -55],
                        class: 'job_flag',
                        title: function (d) {
                            return d[1];
                        },
                        // tooltip: function(d) {return ['<a href="'+d[4]+'"><h3>'+d[1]+'</h3>'+d[3]+'</a>','<img src="'+d[2]+'"/>'];},
                        click: function (d) {
                            //olghelp(d[1],'<a href="'+d[4]+'"><h3>'+d[1]+'</h3>'+d[3]+'</a><br><img src="'+d[2]+'"/><br><input type="button" value="Agenturmeldung"><input type="button" value="Recherche">',d[4], 450, 600); //oli 15.01.14 1:42 wieder einkommentiert
                            //ludwig, nochmal checken bitte.  
                            olghelp(d[1], '<div style="width: 100%; display: inline-block; height: 520px; line-height: 520px; text-align: center;"><img src="http://spiel.fremdeweltganznah.de/fileadmin/htmltest/css/images/animated-overlay.gif"/></div>', d[4], 552, 492, d[5], d[6]);
                            //Über Func:olghelp parameter für h & w aus json oder quelltext übergeben.   
                        }

                    }); //map.addSymbold end

                }); //end jsontest2


            }); //done function end

        //end jsontest



    }); //END: Load Map SVG


    if (($.cookie('mapL'))) { //cookietest
        $('#map').css({
            'left': ($.cookie('mapL')),
            'top': ($.cookie('mapT'))
        });
        //mapposition();
    } else {
        $('#map').css({
            //'left': '-1404px',
            //'top': '-1698px'
            'left': '-1376px',
            'top': '-3060px'
        }); //ca. zentriert // hier sollte als fallback besser die userposi rein!!!
    } // cookietest end


    $('#map').on('click', function () {

        $('.job_flag').draggable();
    });

}); //END: Start Function
