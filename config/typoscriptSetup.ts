# Default PAGE object:
page = PAGE
page.includeCSS.file1 = fileadmin/user_upload/rte.css
page.5 = TEXT
page.5.value = <div class="wrapper">
page.10 = TEXT
page.10 {
  value = Regenwald-Lexikon
wrap = <h1>|</h1>
}
page.20 = HMENU
page.20{
1 = TMENU
1.sectionIndex = 1
1.alternativeSortingField = header
1.NO.allWrap = <b>|</b><br/>
}

page.200 < styles.content.get
page.200.select.orderBy = header
page.5005 = TEXT
page.5005.value = </div>

plugin.tx_jsonapi {
  settings {
    list.orderBy = title
    output {
      config {
       # Example: Model of ext news
        Tx_News_Domain_Model_News {
          _only = uid, pid, title, teaser, bodytext, falMedia, author, authorEmail
          _descend {
            datetime  {
            }
            falMedia {
                _descendAll {
                    _only = title, alternative, description, link, originalResource
                    _descend {
                        originalResource {
                            _only = publicUrl
                        }
                    }
                }
            }
          #  falRelatedFiles < .falMedia
          }
        }
      }
    }
  }
}